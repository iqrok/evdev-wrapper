const EventEmitter = require('events');
const __evdev = require('evdev');

const _CONSTANTS = {
	KEY_1: '1',
	KEY_2: '2',
	KEY_3: '3',
	KEY_4: '4',
	KEY_5: '5',
	KEY_6: '6',
	KEY_7: '7',
	KEY_8: '8',
	KEY_9: '9',
	KEY_0: '0',
	KEY_Q: 'Q',
	KEY_W: 'W',
	KEY_E: 'E',
	KEY_R: 'R',
	KEY_T: 'T',
	KEY_Y: 'Y',
	KEY_U: 'U',
	KEY_I: 'I',
	KEY_O: 'O',
	KEY_P: 'P',
	KEY_A: 'A',
	KEY_S: 'S',
	KEY_D: 'D',
	KEY_F: 'F',
	KEY_G: 'G',
	KEY_H: 'H',
	KEY_J: 'J',
	KEY_K: 'K',
	KEY_L: 'L',
	KEY_Z: 'Z',
	KEY_X: 'X',
	KEY_C: 'C',
	KEY_V: 'V',
	KEY_B: 'B',
	KEY_N: 'N',
	KEY_M: 'M',
};

class __evdevWrapper extends EventEmitter {
	constructor(device, autostart = true){
		super();

		const self = this;
		self._device = device;
		self._str = '';
		self._evdev = new __evdev({
				device: self._device,
			});

		self._timer = {
			last: Date.now(),
			get diff(){
				return Date.now() - this.last;
			},
		};

		if(autostart){
			self.open();
		}
	}

	/**
	 *	emit data if only there is at least 1 listener
	 *	@private
	 *	@param {string} eventName - event name to be emitted
	 *	@param {*} value - event data, any datatype can be emitted
	 * */
	_emit(eventName, ...values){
		const self = this;

		if(self.listenerCount(eventName) > 0){
			self.emit(eventName, ...values);
		}
	}

	open(){
		const self = this;

		self._evdev.on("EV_KEY", function(key){
			if(!key.value){
				return;
			}

			if(key.code === 'KEY_ENTER'){
				self._emit('data', {
					data: self._str,
					diff: self._timer.diff,
				});

				self._str = '';
				self._timer.last = Date.now();

				return;
			}

			self._str += _CONSTANTS[key.code] || '';
		}).on("error", function(e){
			self._emit('error', self._str);
		});

		self._evdev.open(self._device);
	}
}

module.exports = __evdevWrapper;
