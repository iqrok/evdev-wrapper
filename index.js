//~const device = '/dev/input/by-id/usb-HXGCoLtd_HIDKeys-event-kbd';
//~const device = '/dev/input/by-path/pci-0000:00:14.0-usb-0:1:1.0-event-kbd';
const device = '/dev/input/event14';

const EVDEV = require('./evdev-wrapper.lib.js');
const evdev = new EVDEV(device, true);

evdev.on('data', received => {
	if(received.diff < 500){
		return;
	}

	console.log('data', received);
}).on('error', error => {
	console.error('error', error);
});
